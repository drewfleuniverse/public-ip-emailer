<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\BufferedOutput;
//use App\Providers\Artisan;
use App\PublicIP;

class PublicIPsTableSeeder extends Seeder
{
    private $ip;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cmd = "dig +short myip.opendns.com @resolver1.opendns.com";

        $this->ip = trim(shell_exec($cmd));

        PublicIP::create(['ip' => $this->ip, 'queried_at' => \Carbon\Carbon::now()]);
    }
}
