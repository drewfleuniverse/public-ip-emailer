<!DOCTYPE html>
<html>
<head>
    <title>Public IP Chaser</title>
    <meta name="description"
          content="Public IP Chaser notifies the home server admin and registered members the change of dynamic IP. Public IP Chaser is build on top of Laravel.">
    <meta name="title" content="Public IP Chaser">
    <meta name="author" content="Andrew Liu aka Drewfle">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <style>
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>
</head>
<body ng-app="app" ng-controller="MainController">

{{--See: http://plnkr.co/edit/dUGbvnmc089C9rnKFh4f?p=preview--}}
<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

        <button type="button" class="navbar-toggle" ng-init="navCollapsed = true"
                ng-click="navCollapsed = !navCollapsed">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><b><i>PubIP Chaser</i></b></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" ng-class="!navCollapsed && 'in'">

        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Link</a></li>
            <li><a href="#">Register for RubIP Chaser Live Notification</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Link</a></li>
            <li dropdown>
                <a href="#" dropdown-toggle>Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>
<div class="container">

    <div class="col-md-6">
        <div class="text-left">
            <h1>Subscriber List - index</h1>
        </div>

        <div ng-cloak>
            <p>Current Public IP: "<% publicIP.ip %>"</p>
            <p><small><i>For Security reasons, please find the complete IP address in your email inbox or login to <b>PubIP Chaser</b>.</i></small></p>
            <p>Allocation time: <i><% publicIP.changed %></i>.</p>
            <p>Last checked <i><% publicIP.queried %></i> ago.</p>
        </div>
    </div>
    <div class="col-md-6">

    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.13.1/ui-bootstrap-tpls.js"></script>
<script>
    angular
            .module('app', ['ui.bootstrap'], function ($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            })
            .controller('MainController', ['$scope', '$http', function ($scope, $http) {
                $scope.publicIP = [];

                var fetchPublicIP = function () {
                    $http.get('/api/publicip').then(function (response) {
                        $scope.publicIP = response.data;
                    }, function (errResponse) {
                        console.error('Error while fetching IP address');
                    });
                };
                fetchPublicIP();
            }]);
</script>
</body>
</html>
