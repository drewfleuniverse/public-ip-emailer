<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Artisan;
use App\Jobs\UpdatePublicIP;
use App\PublicIP;
use Carbon\Carbon;
class PublicIPController extends Controller
{
    private $publicIP;

    public function __construct() {
        $this->publicIP = PublicIP::orderBy('created_at', 'desc')->first();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        Artisan::call('publicip:get', ['--update' => true]);

        $ip = substr_replace($this->publicIP->ip, '*#$^@', 0 , 5);
        $changed = $this->publicIP->created_at->timezone('America/New_York')->toCookieString();
        $queried = $this->publicIP->queried_at->diffForHumans(Carbon::now(), true);

        $json = [
            'ip' => $ip,
            'changed' => $changed,
            'queried' => $queried
        ];

        return response()->json($json);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
//    public function update(Request $request, $id)
//    {
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
//    public function store(Request $request)
//    {
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function show($id)
//    {
//        //
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function edit($id)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
//    public function destroy($id)
//    {
//        //
//    }
}
