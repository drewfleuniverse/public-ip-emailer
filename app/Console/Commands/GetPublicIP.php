<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\PublicIP;

class GetPublicIP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publicip:get {--update : Update public IP if it has been changed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create or update the public IP of the hosting server in the publicip table';

    protected $publicIP;
    private $oldIP = "";
    private $newIP = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cmd = "dig +short myip.opendns.com @resolver1.opendns.com";
        $this->publicIP = PublicIP::orderBy('created_at', 'desc')->first();
        $this->oldIP = $this->publicIP->ip;

        $curTime = Carbon::now();
        $diff = $this->publicIP->queried_at->diffInMinutes($curTime);

        if ($diff > 10) {
            $this->newIP = trim(shell_exec($cmd));

            if ($this->newIP == "") {
                $this->info('Error fetching public IP.');
            }

            $this->updateIP($this->oldIP, $this->newIP);
            $this->publicIP->queried_at = Carbon::now();
            $this->publicIP->save();
        } else {
            $this->newIP = $this->oldIP;
            $this->updateIP($this->oldIP, $this->newIP);
        }

    }

    private function updateIP($oldIP, $newIP) {
        if ($oldIP != $newIP) {
            $this->info('We have a new public IP: '.$newIP.'. The old IP is: '.$oldIP);
            if ($this->option('update')) {
                $this->info('The new IP has been updated.');
                PublicIP::create(['ip' => $newIP, 'queried_at' => Carbon::now()]);
            }
        } else if ($oldIP == $newIP) {
            $this->info('No new public IP, the IP is remained as '.$oldIP);
        }
    }
}
