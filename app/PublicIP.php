<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicIP extends Model
{
    protected $table = 'public_ips';
    protected $dates = ['created_at', 'updated_at', 'queried_at'];
    protected $fillable = ['ip', 'queried_at'];
}
